const express = require("express");
// this lets us use the mongoose module
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// SECTION - MongoDB connection
/*
	SYNTAX
		mongoose.connect("<MongoDB atlas connection string>", {useNewUrlParser : true, useUnifiedTopology : true})
*/
// Connect to the data by passing in your connection string (from MongoDB)
// ."connect()" lets us connect and access the database that is sent to it via string
// "b190-to-do" is the database that we have created in our MongoDb
mongoose.connect("mongodb+srv://budz956:admin123@wdc028-course-booking.q2syofq.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	// this will not prevent Mongoose from being used in application and sending unnecessary warnings when we send requests
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// sets notifications for connection success or failure
// allows us to handle errors when initial connection is established
// works with on and once mongoose methods
let db = mongoose.connection;
// if a connection error occured, we will be seeing it in the console
// console.error.bind allows us to print errors in the browser as well as in the terminal
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful, confirm it in the console
db.once("open", ()=> console.log("We're connected to the database"));


// SECTION- Schema
// Schema() is a method inside the mongoose module that lets us create schema for our database it recieves an object with properties and data types that each property should have
const taskSchema = new mongoose.Schema( {
	// the "name" property should receive a string data type (the data type should be written in Sentence case)
	name: String,
	status: {
		type: String,
		// the default property allows the server to automatically assign a value to the property once the user fails to provide one
		default: "pending"
	}
} );


// SECTION - Models
// models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use Schemas and they act as the middleman from the server to the database
// First parameter is the collection in where to store the data
// Second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Models must be written in singular form, sentence case
// The "Task" variable now will be used for us to run commands for interacting with our database
const Task = mongoose.model("Task", taskSchema);

// allows handling of json data structure
app.use( express.json() );
// receives all kinds of data
app.use( express.urlencoded( { extended: true} ) );

// Create a new task
/*
	BUSINESS LOGIC
		-add a functionality that will check if there are duplicate tasks
			- if the task is already existing, we return an error
			-if task is not existing, we add it in the database
		-the task will be sent from the request's body
		-create a new Task object with a "name" field/property
		-the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post( "/tasks", ( req,res ) => {
	// findOne is the mongoose method for finding documents in the database. it works similar to ".find()" in MongoDB
	// findOne returns the first document it finds, if there are any; if there are no documents, it will return "null"
	// "err" parameter is for if there are errors that will be encountered during the finding of the documents
	// "result" is for if the finding of documents is successful (whether there are documents or none)
	Task.findOne( { name : req.body.name }, (err, result) =>{
		if ( result !== null && result.name === req.body.name ) {
			return res.send("Duplicate task found");
		}else{
			let newTask = new Task({
				// detects where to get the properties that will be needed to create the new "Task" object to be stored in the database
				// the code says that the "name" property will come from the request body
				name: req.body.name
			});
			// ".save()" is a mongoose method to save the object created in the database
			newTask.save( (saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created");
				};
			});
		};
	} );
} );

/*
	Miniactivity

		Getting all the tasks

		BUSINESS LOGIC
			- retrieve all documents
			- if there are errors, print the error
			- if there are no errors, send a success status back the the client/Postman and return the array of documents

		Send the Postman result in the google chat
*/
app.get( "/tasks", ( req, res ) => {
	Task.find( {  }, ( err, result ) => {
		if ( err ){
			return console.log(err);
		}else{
			return res.status(200).json(result);
			// return res.status(200).json({ data : result}); - creating a new object with "data" property and the value of the result
		};
	} );
} );

/*
c/o Sir Renz
app.get("/tasks",(req,res)=>{
	Task.find({}, (err,result) => {
		if(result === null){
			return res.send("Error")
		}else{
			res.send(result)
		}
	})
})
*/

// SECTION: ACTIVITY

/*
	Business Logic for registering a new user
		- add a functionality to check if there are dupliacte users
			- if the user is already existing, return an error
			- if the user is not existing, save/add the user in the database
		- the user will be coming from the request's body
		- create a new User object with a "username" and "password" fields/properties
*/

// Create a User schema.
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Create a User model.
const User = mongoose.model("User", userSchema)

// app.use( express.json() );
// // receives all kinds of data
// app.use( express.urlencoded( { extended: true} ) );


app.post( "/signup", ( req, res ) => {
	
	User.findOne( { username : req.body.username }, (err, result) =>{
		if ( result !== null && result.username === req.body.name ) {
			return console.log(err);
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save( (saveErr, savedUser) => {
				if (saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send(`Welcome ${req.body.username}`);
				};
			});
		};
	} );
} );



app.listen( port, () => console.log(`Server running at port: ${port}`) );
